
Required libraries / tools
--------------------------

- `CMake 3.5`
- `SDL 2.0`
- `SDL_ttf 2.0`
- `SDL_image 2.0`

Mac OS X, Linux
---------------

Type `make` to make.
Type `doxygen` to generate the documentation.

Windows
-------

Compilation tested on VS2012.

# TODO

- [X] Finish conversion to CMake
- [X] Upgrade engine to SDL2
- [ ] switch to a custom docker image for CI builds to speed up build times
- [ ] get test suite working

# ideas

- npcs who write procedurally generated music and play it on instruments
- robots that talk to each other in procedurally generated languages
