#pragma once

#include <src/types.h>
#include "Tile.h"

struct Creature {
  u64 x;
  u64 y;
  TileOffset image;
};