#pragma once

struct UIWidget {
  int x;
  int y;
  int w;
  int h;
  int color;
};