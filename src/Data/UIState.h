#pragma once

enum UIState {
  NewGame,
  MainMenu,
  Play,
};