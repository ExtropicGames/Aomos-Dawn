#pragma once

struct Camera {
  u64 x, y; // in pixels
  u64 w, h; // in pixels
};