#pragma once

#include <src/types.h>

enum TileType {
  Dirt,
  Grass,
  Water,
};

struct Tile {
  TileType type;
};

struct TileOffset {
  s32 x;
  s32 y;
};

struct TileLayer {
  TileOffset base;
  TileOffset top;
};