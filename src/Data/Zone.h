#pragma once

#include <cassert>
#include <vector>

#include <src/types.h>
#include "Tile.h"

//! A helper class that provides a nicer wrapper around vector<vector<Tile>>
class TileGrid {
public:
  u64 width() {
    return tiles.size();
  }

  u64 height() {
    return tiles[0].size();
  }

  void resize(u64 x, u64 y) {
    tiles.resize(x);
    for (std::vector<Tile> &col : tiles) {
      col.resize(y);
    }
  }

  void set(u64 x, u64 y, TileType type) {
    assert(x < tiles.size());
    assert(y < tiles[x].size());
    tiles[x][y] = {type};
  }

  Tile* get(u64 x, u64 y) {
    if (x >= tiles.size() || y >= tiles[x].size()) {
      return nullptr;
    }

    return &tiles[x][y];
  }
private:
  std::vector<std::vector<Tile>> tiles;
};

struct Zone {
  TileGrid tiles;
};

