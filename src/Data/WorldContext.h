#pragma once

#include <vector>

#include <src/Data/UIState.h>
#include <src/Data/UIWidget.h>
#include <src/Data/Zone.h>
#include "Creature.h"

struct WorldContext {
  Zone* currentZone = nullptr;
  Creature* player = nullptr;

  // UI context
  UIState uiState;
  std::vector<UIWidget> uiWidgets;
};