#pragma once

#include "Region.h"

namespace Gaia {
  //! An interface that other classes can implement in order to get regions for
//! read-only access.
  class Viewer {
  public:
    virtual void setRegion(std::shared_ptr<Region> region) = 0;

    // viewer properties
    virtual u32 x() = 0;    // the dimensions of the viewer measured in tiles
    virtual u32 y() = 0;
    virtual u32 z() = 0;
    virtual u32 w() = 0;
    virtual u32 h() = 0;
    virtual u32 d() = 0;
  };
}
