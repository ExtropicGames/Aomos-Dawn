#pragma once

#include <boost/multi_array.hpp>

#include "Gaia/Cell.h"

namespace Gaia {
  // forward declaration
  class Gaia;

  typedef boost::multi_array<Tile, 3> TileArray;

//! A class that contains a copy of world data that can be manipulated by Viewers and other components.
  class Region {
    friend class Gaia;
  public:
    Region(s32 _x, s32 _y, s32 _z, u32 _w, u32 _h, u32 _d);
    ~Region();

    // TODO: override operator [][][] if possible

    Tile tile(s32 x, s32 y, s32 z);

    void setTile(s32 x, s32 y, s32 z, Tile tile);

  private:
    s32 x;
    s32 y;
    s32 z;
    TileArray tiles;
    bool read_only;
  };

}
