#pragma once

// Gaia headers
#include "Gaia/Cell.h"

namespace Gaia {
  //! An interface for procedural world generators.
  class Creator {
  public:
    virtual void generateCell(const Point &cell, Cell &cell_data) = 0;
  };
}
