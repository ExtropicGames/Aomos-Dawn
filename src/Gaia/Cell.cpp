#include "Cell.h"

namespace Gaia {
  Point getCell(const Point &point) {
    Point result;
    result.x = point.x - (point.x % CELL_DIMENSIONS);
    result.y = point.y - (point.y % CELL_DIMENSIONS);
    result.z = point.z - (point.z % CELL_DIMENSIONS);
    return result;
  }

  Point getSupercell(const Point &point) {
    Point result;
    result.x = point.x - (point.x % SUPERCELL_DIMENSIONS);
    result.y = point.y - (point.y % SUPERCELL_DIMENSIONS);
    result.z = point.z - (point.z % SUPERCELL_DIMENSIONS);
    return result;
  }
}