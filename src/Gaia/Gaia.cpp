#include <sstream>

#include "Gaia.h"

using namespace std;

namespace Gaia {
  Gaia::Gaia(string _saveName, string _worldName, shared_ptr<Creator> _creator) {
    assert(_creator);
    assert(_saveName.size() > 0);
    assert(_worldName.size() > 0);
    saveName = _saveName;
    worldName = worldName;
    creator = _creator;
  }

//! Region is not guaranteed to return a Region that contains *only* the requested area, but it will contain *at least*
//! the requested area.
//! TODO: Full Doxygen comments
// TODO: can use reference counting to ensure that all regions are accounted for at the end of each tick (so no one holds onto them)
  unique_ptr<Region> Gaia::getRegion(s32 x, s32 y, s32 z, s32 w, s32 h, s32 d) {

    // round x & y down to nearest CELL_DIMENSIONS
    x -= x % CELL_DIMENSIONS;
    y -= y % CELL_DIMENSIONS;
    z -= z % CELL_DIMENSIONS;
    // round w & h up to nearest CELL_DIMENSIONS
    w += CELL_DIMENSIONS - (w % CELL_DIMENSIONS);
    h += CELL_DIMENSIONS - (h % CELL_DIMENSIONS);
    d += CELL_DIMENSIONS - (d % CELL_DIMENSIONS);

    unique_ptr<Region> region(new Region(x,y,z,w,h,d));

    // TODO: determine cells in x,y,z,w,h,d
    // TODO: for each cell: check if it is in cache
    // TODO: if yes, copy into region
    // TODO: if not, readCell and then copy into region

    assert(region);
    return region;
  }

//! TODO: Doxygen comments
  bool Gaia::saveRegion(unique_ptr<Region> region) {
    assert(region);

    // TODO: determine if free space in cell cache is large enough to hold region
    // TODO: resize cache if necessary
    // TODO: copy cells into cache, updating timestamp for each

    return false;
  }

// ****************************************************************************
// * Private methods                                                          *
// ****************************************************************************

//! TODO: Doxygen comments
  void Gaia::readCell(const Point &cell) {
    assert(cell.x % CELL_DIMENSIONS == 0);
    assert(cell.y % CELL_DIMENSIONS == 0);
    assert(cell.z % CELL_DIMENSIONS == 0);

    Point supercell = getSupercell(cell);

    string filename = "saves/" + saveName + "/" + worldName + "/firma/" + to_string(supercell.x) + "." + to_string(supercell.y) + "." + to_string(supercell.z) + ".aom";

    // TODO: create directories if not exist: see boost::filesystem::create_directory

    // TODO: cache supercells within gaia to prevent unnecessary reads
    fstream file(filename.c_str(), fstream::in | fstream::out | fstream::binary);

    assert(file);

    Supercell supercell_data;
    file.read((char*) &supercell_data, sizeof(Supercell));

    // TODO: ensure there is enough free space in the cache to hold the cell
    // TODO: if not, increase the cache size

    u32 cell_offset = supercell_data.cellOffsets[(cell.x % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS][(cell.y % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS][(cell.z % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS];

    // Cell does not yet exist, generate it
    if (cell_offset == 0) {
      // TODO: creator->generateCell(cell, &cell_cache[cache_position]);
      return;
    }

    // Cell exists, read it
    file.seekg(cell_offset);
    // TODO: file.read((char*) &cell_cache[cache_position], sizeof(Cell));
  }

//! TODO: Doxygen comments
  void Gaia::writeCell(const Point &cell) {
    assert(cell.x % CELL_DIMENSIONS == 0);
    assert(cell.y % CELL_DIMENSIONS == 0);
    assert(cell.z % CELL_DIMENSIONS == 0);

    Point supercell = getSupercell(cell);

    string filename = "saves/" + saveName + "/" + worldName + "/firma/" + to_string(supercell.x) + "." + to_string(supercell.y) + "." + to_string(supercell.z) + ".aom";

    // TODO: create directories if not exist: see boost::filesystem::create_directory

    // TODO: cache supercells within gaia to prevent unnecessary reads
    fstream file(filename.c_str());

    assert(file);

    Supercell supercell_data;
    file.read((char*) &supercell_data, sizeof(Supercell));

    u32 cell_offset = supercell_data.cellOffsets[(cell.x % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS][(cell.y % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS][(cell.z % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS];

    // Cell does not yet exist, generate it
    if (cell_offset != 0) {
      file.seekg(cell_offset);
      //file.write((char*)
      // TODO: creator->generateCell(cell, &cell_cache[cache_position]);
      return;
    }

    // Cell exists, read it
    file.seekg(cell_offset);
    // TODO: file.read((char*) &cell_cache[cache_position], sizeof(Cell));


    // TODO: check if cell exists in supercell already
    // TODO: if yes, seek to location & write the cell
    // TODO: if no, append to end of file, update the supercell
    // TODO: remove the cell from the cache
  }
}
