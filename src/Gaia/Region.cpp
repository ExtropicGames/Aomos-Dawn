#include "Region.h"

using namespace boost;

namespace Gaia {
  Region::Region(s32 _x, s32 _y, s32 _z, u32 _w, u32 _h, u32 _d) {
    x = _x;
    y = _y;
    z = _z;
    tiles.resize(extents[_w][_h][_d]);
  }

  Region::~Region() {
  }

  Tile Region::tile(s32 _x, s32 _y, s32 _z) {
    assert(_x >= x);
    assert(_y >= y);
    assert(_z >= z);

    return tiles[_x - x][_y - y][_z - z];
  }

  void Region::setTile(s32 _x, s32 _y, s32 _z, Tile tile) {
    assert(!read_only);
    assert(_x >= x);
    assert(_y >= y);
    assert(_z >= z);

    tiles[_x - x][_y - y][_z - z] = tile;
  }
}
