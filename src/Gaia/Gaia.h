#pragma once

#include <fstream>
#include <memory>

#include "Gaia/Region.h"
#include "Gaia/Creator.h"

namespace Gaia {
  //! A class that manages world data.
//! @image html gaia.png
  class Gaia {
  public:
    Gaia(std::string _saveName, std::string _worldName, std::shared_ptr<Creator> _creator);

    std::unique_ptr<Region> getRegion(s32 x, s32 y, s32 z, s32 w, s32 h, s32 d);
    bool saveRegion(std::unique_ptr<Region>);

  private:
    // Private methods
    void readCell(const Point &cell);
    void writeCell(const Point &cell);

    // Private class constants
    static const u32 CELL_CACHE_SIZE = 16;     // Number of cells to keep in memory at once.
    static const u32 SUPERCELL_CACHE_SIZE = 4; // Number of supercells to keep in memory at once.

    // Private instance constants
    // TODO: figure out how to make these immutable
    std::shared_ptr<Creator> creator;
    std::string saveName;      // The savegame that this world is associated with
    std::string worldName;     // The name of the planet that this Gaia watches over

    // Private data
    Cell cell_cache[CELL_CACHE_SIZE];
    Supercell supercell_cache[SUPERCELL_CACHE_SIZE];
    std::fstream supercell_cache_filestreams[SUPERCELL_CACHE_SIZE];
  };
}
