#pragma once

#include "Creator.h"

namespace Gaia {
  struct TileMetadata {
    s32 height;
    //u8 biome;
  };

  struct CellMetadata {
    // TODO: in the future, only keep metadata needed to generate future cells.
    //       flush metadata when it is no longer needed
    TileMetadata tile[CELL_DIMENSIONS][CELL_DIMENSIONS];
  };

  struct SupercellMetadata {
    u32 cellOffset[CELLS_PER_SUPERCELL][CELLS_PER_SUPERCELL];
  };

  struct GaiaMetadata {
    s32 geological_age;
  };

//! The procedural world generator for the planet Aomo.
  class AomoCreator : public Creator {
  public:
    AomoCreator(std::string _saveName, std::string _worldName);

    void generateCell(const Point &cell, Cell &cell_data);
  private:
    bool readMetadata(const Point &cell, CellMetadata &cell_meta);

    std::string saveName;
    std::string worldName;
  };
}
