#include <sstream>
#include <fstream>
#include <assert.h>

#include "AomoCreator.h"

using namespace std;

namespace Gaia {
  AomoCreator::AomoCreator(string _saveName, string _worldName) {
    saveName = _saveName;
    worldName = _worldName;
  }

  void AomoCreator::generateCell(const Point &cell, Cell &cell_data) {
    //TODO: assert(getCell(cell) == cell);

    Point cell_west = cell;
    cell_west.x -= CELL_DIMENSIONS;
    Point cell_east = cell;
    cell_east.x += CELL_DIMENSIONS;
    Point cell_north = cell;
    cell_north.y -= CELL_DIMENSIONS;
    Point cell_south = cell;
    cell_south.y += CELL_DIMENSIONS;

    CellMetadata cell_west_meta;
    CellMetadata cell_east_meta;
    CellMetadata cell_north_meta;
    CellMetadata cell_south_meta;

    bool west_exists = readMetadata(cell_west, cell_west_meta);
    bool east_exists = readMetadata(cell_east, cell_east_meta);
    bool north_exists = readMetadata(cell_north, cell_north_meta);
    bool south_exists = readMetadata(cell_south, cell_south_meta);

    // TODO: look up perlin noise heightmap function and implement it here

    // TODO: pull data from non-empty neighbors and use it to populate temporary procedural data
    // TODO: use temporary procedural data to generate cell tiles
    // TODO: write this cell's CellMetadata to disk
    // TODO: check neighbors to see if their CellMetadata can be deleted
    // TODO: populate Cell return values
  }

//! Returns true if the cell metadata was successfully read.
//! Returns false if the cell metadata did not exist.
  bool AomoCreator::readMetadata(const Point &cell, CellMetadata &cell_meta) {
    //TODO: assert(getCell(cell) == cell);

    Point supercell = getSupercell(cell);

    string filename = "saves/"+ saveName + "/" + worldName + "/creation/" + to_string(supercell.x) + "." + to_string(supercell.y) + ".prc";

    // TODO: create file if not exist

    // TODO: cache file handles within creator to prevent unnecessary opening/closing/reading header blocks etc.
    fstream file(filename.c_str());

    assert(file);

    SupercellMetadata supercell_meta;
    file.read((char*) &supercell_meta, sizeof(SupercellMetadata));

    u32 cell_offset = supercell_meta.cellOffset[(cell.x % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS][(cell.y % SUPERCELL_DIMENSIONS) / CELL_DIMENSIONS];

    if (cell_offset == 0) {
      file.close();
      return false;
    }

    file.seekg(cell_offset);
    file.read((char*) &cell_meta, sizeof(CellMetadata));

    file.close();
    return true;
  }
}