#pragma once

#include "types.h"

namespace Gaia {
  const u32 SUPERCELL_DIMENSIONS = 1024;  // The number of tiles per side of a supercell.
  const u32 CELL_DIMENSIONS = 128;        // The number of tiles per "side" of a cell.
  const u32 CELLS_PER_SUPERCELL = SUPERCELL_DIMENSIONS / CELL_DIMENSIONS;

  struct Tile {
    u8 data;
  };

  struct Cell {
    u64 lastUpdated;
    Tile tile[CELL_DIMENSIONS][CELL_DIMENSIONS][CELL_DIMENSIONS];
  };

  // Flags:
  // 0x1 - Indicates cells are zlib compressed.
  struct Supercell {
    c8 magicCookie[4];
    u8 versionNumber;
    u8 flags;
    u32 CRC;
    s32 cellOffsets[CELLS_PER_SUPERCELL][CELLS_PER_SUPERCELL][CELLS_PER_SUPERCELL];
  };

  Point getCell(const Point &point);
  Point getSupercell(const Point &point);
}