// Gaia includes
#include "Gaia/Gaia.h"
#include "Gaia/AomoCreator.h"

// Engine includes
#include <src/Engine/Log.h>
#include <src/Engine/Renderer.h>
#include <src/Engine/InputReader.h>
#include <src/Engine/GameLogic.h>
#include <src/Data/Zone.h>

using namespace std;
using namespace Extropic;

// Must have argc and argv for SDL 1.2
int main(int argc, char** argv) {

  Log::trace("Entering main");

  shared_ptr<Gaia::Gaia> gaia(new Gaia::Gaia("save1", "aomo", shared_ptr<Gaia::Creator>(new Gaia::AomoCreator("save1", "aomo"))));

  GraphicsContext graphicsContext = initGraphicsContext("Histories of Aomo: Rise of Empire 0.0.1", 640, 480);
  WorldContext worldContext;
  Camera playerCamera;
  playerCamera.w = 320;
  playerCamera.h = 240;
  Zone mainZone = Zone();

  Creature player;
  player.image = {0, 0};
  player.x = 0;
  player.y = 0;
  worldContext.player = &player;

  mainZone.tiles.resize(64, 64);

  for (u64 r = 0; r < 64; r++) {
    for (u64 c = 0; c < 64; c++) {
      int x = rand() % 3;
      switch (x) {
        case 0:
          mainZone.tiles.set(r, c, Dirt);
          break;
        case 1:
          mainZone.tiles.set(r, c, Grass);
          break;
        default:
          mainZone.tiles.set(r, c, Water);
      }
    }
  }

  worldContext.currentZone = &mainZone;

  // main game loop
  while (true) {
    render(&graphicsContext, &worldContext, &playerCamera);
    Action action = readInput();
    if (action.type == Quit) {
      break;
    }
    update(action, &worldContext);
  }

  deinitGraphicsContext(&graphicsContext);

  return 0;
}
