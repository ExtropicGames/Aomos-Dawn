#pragma once

#include <SDL2/SDL.h>

#include "src/Data/WorldContext.h"
#include "src/Data/Camera.h"

//! Acting as an interface between the game engine and the underlying graphics library.
//! This struct encapsulates all SDL-specific code.
struct GraphicsContext {
  SDL_Window* window;
  SDL_Surface* surface;
  SDL_Renderer* renderer;

  SDL_Texture* zoneTileset;
  SDL_Texture* creatureTileset;
  int countedFrames;
};

//! Initialize a new GraphicsContext
GraphicsContext initGraphicsContext(std::string windowTitle, int width, int height);

//! Tear down the context
void deinitGraphicsContext(GraphicsContext* graphicsContext);

//! Render the given camera's current view
void render(GraphicsContext* graphicsContext, WorldContext* worldContext, Camera* camera);
