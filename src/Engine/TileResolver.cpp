
#import <src/Engine/TileResolver.h>

TileOffset resolveBaseLayer(Tile* t);

TileLayer resolveTileLayer(Tile* nw, Tile* n, Tile* ne, Tile* w, Tile* c, Tile* e, Tile* sw, Tile* s, Tile* se) {
  assert(c != nullptr);

  TileLayer result;

  result.top = resolveBaseLayer(c);
  switch (c->type) {
    case Dirt:
      result.top = {8, 10};
      break;
    case Grass:
      result.top = {3, 16};
      break;
    case Water:
    default:
      result.top = {3, 1};
      break;
  }

  result.base = {0, 2};

  if (n != nullptr && n->type > c->type) {
    result.top.y--;
    result.base = resolveBaseLayer(n);
  } else if (s != nullptr && s->type > c->type) {
    result.top.y++;
    result.base = resolveBaseLayer(s);
  }

  if (e != nullptr && e->type > c->type) {
    result.top.x++;
    result.base = resolveBaseLayer(e);
  } else if (w != nullptr && w->type > c->type) {
    result.top.x--;
    result.base = resolveBaseLayer(w);
  }

  return result;
}

TileOffset resolveBaseLayer(Tile* t) {
  assert(t != nullptr);

  switch (t->type) {
    case Dirt:
      return {8, 10};
    case Grass:
      return {3, 16};
    case Water:
    default:
      return {3, 1};
  }
}
