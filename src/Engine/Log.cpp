#include "Log.h"

using namespace std;

//! @TODO test color values
const string TEXT_NORMAL = "\033[0m";
const string TEXT_RED = "\033[0;31m";
const string TEXT_GREEN = "\033[0;32m";
const string TEXT_YELLOW = "\033[0;33m";
const string TEXT_BLUE = "\033[0;34m";
const string TEXT_MAGENTA = "\033[0;35m";
const string TEXT_CYAN = "\033[0;36m";
const string TEXT_GRAY = "\033[0;37m";

namespace Extropic {
  namespace Log {
    void trace(std::string msg) {
      cout << TEXT_GRAY << msg << TEXT_NORMAL << endl << flush;
    }
    void info(std::string msg) {
      cout << TEXT_CYAN << msg << TEXT_NORMAL << endl << flush;
    }
    void warning(std::string msg) {
      cout << TEXT_YELLOW << msg << TEXT_NORMAL << endl << flush;
    }
    void error(std::string msg) {
      cout << TEXT_RED << msg << TEXT_NORMAL << endl << flush;
    }
  }
}
