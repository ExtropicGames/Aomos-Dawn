
#include <cassert>

#include <src/Engine/Log.h>
#include <src/Engine/TileResolver.h>
#include <src/Data/Zone.h>
#include <src/Data/UIWidget.h>
#include <src/types.h>
#include <SDL_image.h>

#include "Renderer.h"

using namespace std;
using namespace Extropic;

const int TILE_WIDTH = 16;
const int TILE_HEIGHT = 16;

// Private functions
void renderWidget(GraphicsContext* graphicsContext, UIWidget* widget);
string to_string(SDL_Rect rect);

GraphicsContext initGraphicsContext(string windowTitle, int width, int height) {

  // TODO: could change to SDL_INIT_VIDEO if we want to manage graphics separately
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    Log::error("SDL could not be initialized: " + string(SDL_GetError()));
    exit(0); // TODO: cleaner exit?
  }

  // TODO: init TTF?

  GraphicsContext gCtx;

  gCtx.window = SDL_CreateWindow(windowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
  if (gCtx.window == nullptr) {
    Log::error("SDL window could not be created: " + string(SDL_GetError()));
    exit(0); // TODO: cleaner exit?
  }

  gCtx.renderer = SDL_CreateRenderer(gCtx.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (gCtx.renderer == nullptr) {
    Log::error("SDL renderer could not be created: " + string(SDL_GetError()));
    exit(0); // TODO: cleaner exit?
  }
  SDL_RenderSetScale(gCtx.renderer, 2.0, 2.0);

  gCtx.surface = SDL_GetWindowSurface(gCtx.window);

  SDL_Surface* tileset = IMG_Load("data/tileset.png");
  gCtx.zoneTileset = SDL_CreateTextureFromSurface(gCtx.renderer, tileset);
  SDL_FreeSurface(tileset);

  tileset = IMG_Load("data/characters.png");
  gCtx.creatureTileset = SDL_CreateTextureFromSurface(gCtx.renderer, tileset);
  SDL_FreeSurface(tileset);

  return gCtx;
}

void deinitGraphicsContext(GraphicsContext* graphicsContext) {
  SDL_DestroyWindow(graphicsContext->window);
  SDL_Quit();
}

void render(GraphicsContext* graphicsContext, WorldContext* worldContext, Camera* camera) {
  assert(graphicsContext != nullptr);
  assert(worldContext != nullptr);
  assert(camera != nullptr);

  u64 startX = camera->x / TILE_WIDTH;
  u64 endX = camera->w / TILE_WIDTH;
  u64 startY = camera->y / TILE_HEIGHT;
  u64 endY = camera->h / TILE_HEIGHT;

  if (worldContext->currentZone != nullptr) {

    Zone* zone = worldContext->currentZone;

    for (u64 x = startX; x < endX; x++) {
      for (u64 y = startY; y < endY; y++) {
        TileLayer layer = resolveTileLayer(
            zone->tiles.get(x-1, y-1),
            zone->tiles.get(x, y-1),
            zone->tiles.get(x+1, y-1),
            zone->tiles.get(x-1, y),
            zone->tiles.get(x, y),
            zone->tiles.get(x+1, y),
            zone->tiles.get(x-1, y+1),
            zone->tiles.get(x, y+1),
            zone->tiles.get(x+1, y+1)
        );

        SDL_Rect sourceRect = { layer.base.x * TILE_WIDTH, layer.base.y * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT };
        SDL_Rect destRect = {(int) ((x - startX) * TILE_WIDTH), (int) ((y - startY) * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT };
        SDL_RenderCopy(graphicsContext->renderer, graphicsContext->zoneTileset, &sourceRect, &destRect);

        sourceRect = { layer.top.x * TILE_WIDTH, layer.top.y * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT };
        SDL_RenderCopy(graphicsContext->renderer, graphicsContext->zoneTileset, &sourceRect, &destRect);
      }
    }
  }

  // Render player and creatures
  SDL_Rect sourceRect = { worldContext->player->image.x * TILE_WIDTH, worldContext->player->image.y * TILE_HEIGHT, TILE_WIDTH, TILE_WIDTH};
  SDL_Rect destRect = {(int) ((worldContext->player->x - startX) * TILE_WIDTH),
                       (int) ((worldContext->player->y - startY) * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT};
  SDL_RenderCopy(graphicsContext->renderer, graphicsContext->creatureTileset, &sourceRect, &destRect);

  for (UIWidget widget : worldContext->uiWidgets) {
    renderWidget(graphicsContext, &widget);
  }

  SDL_RenderPresent(graphicsContext->renderer);

  // calculate and display FPS
  graphicsContext->countedFrames++;
  if (graphicsContext->countedFrames % 100 == 0) {
    float seconds = (SDL_GetTicks() / 1000.0f);
    Log::info("seconds=" + to_string(seconds) + " frames=" + to_string(graphicsContext->countedFrames) + " fps=" + to_string(graphicsContext->countedFrames / seconds));
  }
}

string to_string(SDL_Rect rect) {
  return "{" + to_string(rect.x) + ", " + to_string(rect.y) + ", " + to_string(rect.w) + ", " + to_string(rect.h) + "}";
}

///////////////////////
//                   //
// Private functions //
//                   //
///////////////////////

void renderWidget(GraphicsContext* graphicsContext, UIWidget* widget) {
  // TODO
}