#pragma once

enum ActionType {
  None,
  Quit,
  North,
  South,
  East,
  West,
};

struct Action {
  ActionType type;
};

Action readInput();