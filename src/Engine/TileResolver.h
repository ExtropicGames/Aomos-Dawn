#pragma once

#include <SDL_image.h>
#include <src/Data/Zone.h>

TileLayer resolveTileLayer(Tile* nw, Tile* n, Tile* ne, Tile* w, Tile* c, Tile* e, Tile* sw, Tile* s, Tile* se);