#include <string>

#include "GameLogic.h"

using namespace std;

bool isBlocked(WorldContext* worldContext, u64 x, u64 y);

void update(Action action, WorldContext* worldContext) {

  switch (action.type) {
    case North:
      worldContext->player->y--;
      if (isBlocked(worldContext, worldContext->player->x, worldContext->player->y)) {
        worldContext->player->y++;
      }
      break;
    case South:
      worldContext->player->y++;
      if (isBlocked(worldContext, worldContext->player->x, worldContext->player->y)) {
        worldContext->player->y--;
      }
      break;
    case East:
      worldContext->player->x--;
      if (isBlocked(worldContext, worldContext->player->x, worldContext->player->y)) {
        worldContext->player->x++;
      }

      break;
    case West:
      worldContext->player->x++;
      if (isBlocked(worldContext, worldContext->player->x, worldContext->player->y)) {
        worldContext->player->x--;
      }
      break;
  }
}

bool isBlocked(WorldContext* world, u64 x, u64 y) {
  Tile* t = world->currentZone->tiles.get(x, y);
  if (t->type == Water) {
    return true;
  }

  return false;
}