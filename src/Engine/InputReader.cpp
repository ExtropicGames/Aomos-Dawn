
#include <string>

#include <SDL2/SDL.h>

#include <src/Engine/InputReader.h>
#include <src/Engine/Log.h>

using namespace std;

Action readInput() {
  Action action;
  action.type = None;

  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      action.type = Quit;
      break;
    }
    if (event.type == SDL_KEYDOWN) {
      switch (event.key.keysym.sym) {
        case SDLK_DOWN:
          action.type = South;
          break;
        case SDLK_UP:
          action.type = North;
          break;
        case SDLK_LEFT:
          action.type = East;
          break;
        case SDLK_RIGHT:
          action.type = West;
          break;
      }
      break;
    }
  }

  return action;
}