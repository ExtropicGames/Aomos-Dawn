#pragma once

#include <iostream>
#include <sstream>

namespace Extropic {
  namespace Log {
    void trace(std::string);
    void info(std::string);
    void warning(std::string);
    void error(std::string);
  }

}
