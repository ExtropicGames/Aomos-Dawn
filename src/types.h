#pragma once

#define null 0

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
#ifdef _WIN32
typedef unsigned long long u64;
#else
typedef unsigned long u64;
#endif

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
#ifdef _WIN32
typedef signed long long s64;
#else
typedef signed long s64;
#endif

typedef float f32;
typedef double f64;

typedef char c8;

static_assert(sizeof(u8) == 1, "u8 is not 1 byte.");
static_assert(sizeof(u16) == 2, "u16 is not 2 bytes.");
static_assert(sizeof(u32) == 4, "u32 is not 4 bytes.");
static_assert(sizeof(u64) == 8, "u64 is not 8 bytes.");
static_assert(sizeof(s8) == 1, "s8 is not 1 byte.");
static_assert(sizeof(s16) == 2, "s16 is not 2 bytes.");
static_assert(sizeof(s32) == 4, "s32 is not 4 bytes.");
static_assert(sizeof(s64) == 8, "s64 is not 8 bytes.");
static_assert(sizeof(f32) == 4, "f32 is not 4 bytes.");
static_assert(sizeof(f64) == 8, "f64 is not 8 bytes.");
static_assert(sizeof(c8) == 1, "c8 is not 1 byte.");

// TODO: think of a better name than Point
struct Point {
    s32 x;
    s32 y;
    s32 z;
};
