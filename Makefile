all:
	@mkdir -p build
	cd build && cmake .. && make && mv AomosDawn ..

clean:
	rm -rf build
	rm AomosDawn